use strict;
use warnings;

#this forces autoflush / will not buffer output. Most of the times output will not be buffered, this only protects against it, if it happens.
$| = 1;


sub main{

    my @files = (
        'C:\Users\adrij\Desktop\learn-perl-5-by-doing-it-2023\lesson 3\logo.png',
        'C:\Users\adrij\Desktop\learn-perl-5-by-doing-it-2023\lesson 3\home.html',
        'C:\Users\adrij\Desktop\learn-perl-5-by-doing-it-2023\lesson 3\missing.txt',
    );
    
    # '-f' checks if there is a file by that name/location
    foreach my $file(@files){
        if(-f $file ){
            print("File found $file\n");
        }
        else{
            print("File $file not found\n");
        }
    }

    
}

main();