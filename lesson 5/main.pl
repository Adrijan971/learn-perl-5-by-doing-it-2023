use strict;
use warnings;

$|=1;

#simple example of reading a file and checking if some simple regex works
sub main{
    my $file = 'C:\Users\adrij\Desktop\learn-perl-5-by-doing-it-2023\lesson 5/text.txt';

    open(INPUT, $file) or die("file $file not found"); 
    
    while(my $line = <INPUT>){
        if ($line =~ /line/){
            print('contains word "line"'."\n");
        }
    }

    close(INPUT);

    #print(<INPUT>);
}

main();